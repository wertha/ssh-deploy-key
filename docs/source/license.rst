License
*******

License Version
===============

SSH deploy key is licensed under version 2.1 of the 
GNU Lesser GPL.


License Text
============

The full text of the license is available here:

http://www.gnu.org/licenses/lgpl-2.1.txt

