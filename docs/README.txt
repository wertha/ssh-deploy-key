The ssh-deploy-key documentation is built with Sphinx.  To build
the documentation yourself, you will need to install both sphinx and
sphinxcontrib-fulltoc.

    pip instal Sphinx

    pip install sphinxcontrib-fulltoc

    make html

That should do it!

